#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "modules/Lock.h"

#include "inc/hw_ssi.h"
#include "driverlib/ssi.h"
#include "driverlib/interrupt.h"

//*****************************************************************************
//
// Number of bytes to send and receive.
//
//*****************************************************************************
#define NUM_SSI_DATA 8

//*****************************************************************************
//
// Global variables used in interrupt handler and the main loop.
//
//*****************************************************************************
volatile unsigned long g_ulSSI0RXTO = 0;
uint32_t g_ulDataRx0[NUM_SSI_DATA];
char buf[16];
Lock lock;

//*****************************************************************************
//
// Interrupt handler for SSI2 peripheral in slave mode.  It reads the interrupt
// status and if the interrupt is fired by a RX time out interrupt it reads the
// SSI2 RX FIFO and increments a counter to tell the main loop that RX timeout
// interrupt was fired.
//
//*****************************************************************************

uint32_t g_ui32SysClock;
uint32_t g_ulDataTx1[8] = {1, 2, 3, 4, 5, 6, 7, 8}; 
uint32_t g_ulDataRx1[8];
uint32_t ulStatus = 0;
uint32_t ui32Index = 0;
volatile uint32_t transferCount = 1;
volatile char led_status = 0;

void SSI0IntHandler()
{
    led_status ^= 1;

	ulStatus = SSIIntStatus(SSI0_BASE, 1);
    SSIIntDisable(SSI0_BASE, SSI_RXFF);
    UARTprintf("\n|Tx1 = %d\n", g_ulDataTx1[0]);
	if(ulStatus & SSI_RXFF)
	{
       UARTprintf("\nSPI Transfer %d\n", transferCount);

        transferCount++;
		for(ui32Index = 0; ui32Index < NUM_SSI_DATA; ui32Index++){

		   SSIDataGet(SSI0_BASE, &g_ulDataRx1[ui32Index]); //Receive
           SSIDataPut(SSI0_BASE, g_ulDataTx1[ui32Index]);//Write

		   //
		   // Since we are using 8-bit data, mask off the MSB.
		   //
		   g_ulDataRx1[ui32Index] &= 0x00FF;
		   //
		   // Display the data that SSI0 received.
		   //
		   UARTprintf("Data[%d] is: '%c' \n",ui32Index , g_ulDataRx1[ui32Index]);
		}        
	}

    SSIIntEnable(SSI0_BASE, SSI_RXFF);
	SSIIntClear(SSI0_BASE, ulStatus);


}

void
InitSPI0(void)
{

    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    GPIOPinConfigure(GPIO_PA2_SSI0CLK);
    GPIOPinConfigure(GPIO_PA3_SSI0FSS);
    GPIOPinConfigure(GPIO_PA4_SSI0RX);
    GPIOPinConfigure(GPIO_PA5_SSI0TX);


    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_5 | GPIO_PIN_4 | GPIO_PIN_3 |
                   GPIO_PIN_2);

    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_3,
    				   SSI_MODE_SLAVE, 8000000, 8);

    SSIEnable(SSI0_BASE);
}


//*****************************************************************************
//
// GPIO registers (PORTB)
//
//*****************************************************************************
#define GPIO_PORTB_DATA_BITS_R  ((volatile uint32_t *)0x40005000)
#define GPIO_PORTB_DATA_R       (*((volatile uint32_t *)0x400053FC))
#define GPIO_PORTB_DIR_R        (*((volatile uint32_t *)0x40005400))
#define GPIO_PORTB_IS_R         (*((volatile uint32_t *)0x40005404))
#define GPIO_PORTB_IBE_R        (*((volatile uint32_t *)0x40005408))
#define GPIO_PORTB_IEV_R        (*((volatile uint32_t *)0x4000540C))
#define GPIO_PORTB_IM_R         (*((volatile uint32_t *)0x40005410))
#define GPIO_PORTB_RIS_R        (*((volatile uint32_t *)0x40005414))
#define GPIO_PORTB_MIS_R        (*((volatile uint32_t *)0x40005418))
#define GPIO_PORTB_ICR_R        (*((volatile uint32_t *)0x4000541C))
#define GPIO_PORTB_AFSEL_R      (*((volatile uint32_t *)0x40005420))
#define GPIO_PORTB_DR2R_R       (*((volatile uint32_t *)0x40005500))
#define GPIO_PORTB_DR4R_R       (*((volatile uint32_t *)0x40005504))
#define GPIO_PORTB_DR8R_R       (*((volatile uint32_t *)0x40005508))
#define GPIO_PORTB_ODR_R        (*((volatile uint32_t *)0x4000550C))
#define GPIO_PORTB_PUR_R        (*((volatile uint32_t *)0x40005510))
#define GPIO_PORTB_PDR_R        (*((volatile uint32_t *)0x40005514))
#define GPIO_PORTB_SLR_R        (*((volatile uint32_t *)0x40005518))
#define GPIO_PORTB_DEN_R        (*((volatile uint32_t *)0x4000551C))
#define GPIO_PORTB_LOCK_R       (*((volatile uint32_t *)0x40005520))
#define GPIO_PORTB_CR_R         (*((volatile uint32_t *)0x40005524))
#define GPIO_PORTB_AMSEL_R      (*((volatile uint32_t *)0x40005528))
#define GPIO_PORTB_PCTL_R       (*((volatile uint32_t *)0x4000552C))
#define GPIO_PORTB_ADCCTL_R     (*((volatile uint32_t *)0x40005530))
#define GPIO_PORTB_DMACTL_R     (*((volatile uint32_t *)0x40005534))
#define SYSCTL_RCGC2_R          (*((volatile uint32_t *)0x400FE108))

//*****************************************************************************
//
// SSI Registers
//
//*****************************************************************************
#define SYSCTL_RCGCSSI_R        (*((volatile uint32_t *)0x400FE61C))
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08))
#define GPIO_PORTA_AFSEL_R      (*((volatile uint32_t *)0x40004420))
#define GPIO_PORTA_DEN_R        (*((volatile uint32_t *)0x4000451C))
#define GPIO_PORTA_PCTL_R       (*((volatile uint32_t *)0x4000452C))
#define GPIO_PORTA_AMSEL_R      (*((volatile uint32_t *)0x40004528))
#define SSI0_CR1_R              (*((volatile uint32_t *)0x40008004))
#define SSI0_CPSR_R             (*((volatile uint32_t *)0x40008010))
#define SSI0_CR0_R              (*((volatile uint32_t *)0x40008000))
#define SSI0_DR_R               (*((volatile uint32_t *)0x40008008))
#define SSI0_SR_R               (*((volatile uint32_t *)0x4000800C))
#define INT_SSI0                23          // SSI0


void ConfigureUART(void)
{   
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);


    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    UARTStdioConfig(0, 115200, 16000000);
}


void portB_Init(void){ 
  volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000002;   // activate clock for Port B
  delay = SYSCTL_RCGC2_R;   // allow time for clock to start
  GPIO_PORTB_AMSEL_R = 0x00;
  GPIO_PORTB_AFSEL_R = 0x00;
  GPIO_PORTB_PCTL_R = 0x00000000;
  GPIO_PORTB_DIR_R = 0x01;   // PB0 Output, PB1-7 Input 
  GPIO_PORTB_CR_R = 0xFF;
  GPIO_PORTB_PUR_R = 0xFE;
  GPIO_PORTB_DEN_R = 0xFF;   // enable digital port
}


//---SYSTEM CONTROL REGISTERS---//
#define SYS_CTRL_RCGC2  (*((volatile unsigned long *)0x400FE108))   //offset of RCGC2 register is 0x108
#define CLK_GPIOF   0x20
//---GPIO-F REGISTER---//
#define PORTF_DATA  (*((volatile unsigned long *)0x40025038))   //offset of DATA register for PF1, PF2, PF3 is 0x38 [PF7:PF0::9:2]
#define PORTF_DIR   (*((volatile unsigned long *)0x40025400))   //offset of DIR register is 0x400
#define PORTF_DEN   (*((volatile unsigned long *)0x4002551C))   //offset of DEN register is 0x51C
//---PORT-F I/O---//
#define PF1 0x02
#define PF2 0x04
#define PF3 0x08

void InitLock(Lock* lock, int combination, char cPin){
    static LockPin p1(1), p2(2), p3(3), p4(4), p5(5), p6(6), p7(7);
    lock->addPin(p1); lock->addPin(p2); lock->addPin(p3);
    lock->addPin(p4); lock->addPin(p5); lock->addPin(p6);
    lock->addPin(p7);
    lock->setCombination(combination);
    lock->setConfirmPin(cPin);
}

void portF_led_Init(void){
    SYS_CTRL_RCGC2 |= CLK_GPIOF;
   PORTF_DIR |= 0x0000000E;    //set PF1, PF2, PF3 as output
   PORTF_DEN |= 0x0000000E;    //enable PF1, PF2, PF3

   }

void PortBIntHandler(){
        if((GPIO_PORTB_DATA_R & ~0x1) != 0){
            lock.unlockSequence(GPIO_PORTB_DATA_R & ~0x1);
        }
        
}

int main(){
    SysCtlClockSet(SYSCTL_SYSDIV_2_5| SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ |SYSCTL_OSC_MAIN);

    portB_Init();

    ConfigureUART();
    UARTprintf("Receiving on SSI0 ...\n\r");

    InitLock(&lock, 4561, 5);
    
    GPIOIntRegister(GPIO_PORTB_BASE,PortBIntHandler);
    GPIOIntTypeSet(GPIO_PORTB_BASE, 0xFE, GPIO_HIGH_LEVEL);
    GPIOIntEnable(GPIO_PORTB_BASE, 0xFE);
    
    InitSPI0();
         
    SSIIntEnable(SSI0_BASE, SSI_RXFF);
    SSIIntRegister(SSI0_BASE, SSI0IntHandler); // int enable new
    SSIIntClear(SSI0_BASE, SSI_RXFF);   
    
    UARTprintf("Receiving on SSI0 ...\n\r");
    
    while(1){

    }
        
    return 0;
}