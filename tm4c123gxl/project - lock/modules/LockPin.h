class LockPin{
    private:
        char key;
        unsigned char value;
    public:
        LockPin(char pin);
        char getStatus(void);
        char getKey(void);
        unsigned char getValue(void);
};
