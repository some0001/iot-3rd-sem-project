#include "LockPin.h"

#include <stdint.h>
#define GPIO_PORTB_DATA_R       (*((volatile uint32_t *)0x400053FC))


LockPin::LockPin(char pin)
{
    this->key = pin;
    switch(pin)
    {
            case 1:
                this->value = 0x2;
                break;
            case 2:
                this->value = 0x4;
                break;
            case 3:
                this->value = 0x8;
                break;
            case 4:
                this->value = 0x10;
                break;
            case 5:
                this->value = 0x20;
                break;
            case 6:
                this->value = 0x40;
                break;
            case 7:
                this->value = 0x80;
                break;
    }
}

char LockPin::getStatus(void)
{   
    return (GPIO_PORTB_DATA_R & ~0x1) == this->value;
}
char LockPin::getKey(void){
    return this->key;
}
unsigned char LockPin::getValue(void){
    return this->value;
}