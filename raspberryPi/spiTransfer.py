import time
import spidev


spi = spidev.SpiDev()
spi.open(0, 0)
spi.no_cs = True
spi.bits_per_word = 8
spi.max_speed_hz = 1000000
spi.mode = 0b11


while True:
    rxBuf = spi.xfer([1,2,3,4,5,6,7,254])

    print(rxBuf)

    time.sleep(1)
