
#include "LockPin.h"
#include <vector>
#include <string>

#define pinCount 8
#define maxCodeLength 32
#define timeout 9999999

class Lock{
    private:
        char status; // 0: Locked | 1: Unlocked
        char codeLength;
        LockPin* lockPins[pinCount];
        char code[maxCodeLength];
    public:
        Lock();
        char setCode(int code);
        char addCodeDigit(char digit);
        char getCodeLength(void);
        void clearCode(void);
        char addPin(LockPin& pin);
        char checkPin(char pos);
        char unlockSequence(void);
        char unlockLock(void);
        char lockLock(void);
};
