#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"
#include "modules/Lock.h"

//*****************************************************************************
//
// GPIO registers (PORTB)
//
//*****************************************************************************
#define GPIO_PORTB_DATA_BITS_R  ((volatile uint32_t *)0x40005000)
#define GPIO_PORTB_DATA_R       (*((volatile uint32_t *)0x400053FC))
#define GPIO_PORTB_DIR_R        (*((volatile uint32_t *)0x40005400))
#define GPIO_PORTB_IS_R         (*((volatile uint32_t *)0x40005404))
#define GPIO_PORTB_IBE_R        (*((volatile uint32_t *)0x40005408))
#define GPIO_PORTB_IEV_R        (*((volatile uint32_t *)0x4000540C))
#define GPIO_PORTB_IM_R         (*((volatile uint32_t *)0x40005410))
#define GPIO_PORTB_RIS_R        (*((volatile uint32_t *)0x40005414))
#define GPIO_PORTB_MIS_R        (*((volatile uint32_t *)0x40005418))
#define GPIO_PORTB_ICR_R        (*((volatile uint32_t *)0x4000541C))
#define GPIO_PORTB_AFSEL_R      (*((volatile uint32_t *)0x40005420))
#define GPIO_PORTB_DR2R_R       (*((volatile uint32_t *)0x40005500))
#define GPIO_PORTB_DR4R_R       (*((volatile uint32_t *)0x40005504))
#define GPIO_PORTB_DR8R_R       (*((volatile uint32_t *)0x40005508))
#define GPIO_PORTB_ODR_R        (*((volatile uint32_t *)0x4000550C))
#define GPIO_PORTB_PUR_R        (*((volatile uint32_t *)0x40005510))
#define GPIO_PORTB_PDR_R        (*((volatile uint32_t *)0x40005514))
#define GPIO_PORTB_SLR_R        (*((volatile uint32_t *)0x40005518))
#define GPIO_PORTB_DEN_R        (*((volatile uint32_t *)0x4000551C))
#define GPIO_PORTB_LOCK_R       (*((volatile uint32_t *)0x40005520))
#define GPIO_PORTB_CR_R         (*((volatile uint32_t *)0x40005524))
#define GPIO_PORTB_AMSEL_R      (*((volatile uint32_t *)0x40005528))
#define GPIO_PORTB_PCTL_R       (*((volatile uint32_t *)0x4000552C))
#define GPIO_PORTB_ADCCTL_R     (*((volatile uint32_t *)0x40005530))
#define GPIO_PORTB_DMACTL_R     (*((volatile uint32_t *)0x40005534))
#define SYSCTL_RCGC2_R          (*((volatile uint32_t *)0x400FE108))

//*****************************************************************************
//
// SSI Registers
//
//*****************************************************************************
#define SYSCTL_RCGCSSI_R        (*((volatile uint32_t *)0x400FE61C))
#define SYSCTL_RCGCGPIO_R       (*((volatile uint32_t *)0x400FE608))
#define SYSCTL_PRGPIO_R         (*((volatile uint32_t *)0x400FEA08))
#define GPIO_PORTA_AFSEL_R      (*((volatile uint32_t *)0x40004420))
#define GPIO_PORTA_DEN_R        (*((volatile uint32_t *)0x4000451C))
#define GPIO_PORTA_PCTL_R       (*((volatile uint32_t *)0x4000452C))
#define GPIO_PORTA_AMSEL_R      (*((volatile uint32_t *)0x40004528))
#define SSI0_CR1_R              (*((volatile uint32_t *)0x40008004))
#define SSI0_CPSR_R             (*((volatile uint32_t *)0x40008010))
#define SSI0_CR0_R              (*((volatile uint32_t *)0x40008000))
#define SSI0_DR_R               (*((volatile uint32_t *)0x40008008))
#define SSI0_SR_R               (*((volatile uint32_t *)0x4000800C))


void ConfigureUART(void)
{   
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);

    ROM_GPIOPinConfigure(GPIO_PA0_U0RX);
    ROM_GPIOPinConfigure(GPIO_PA1_U0TX);
    ROM_GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);


    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    UARTStdioConfig(0, 115200, 16000000);
}


void spiMaster_Init(uint16_t data){
    SYSCTL_RCGCSSI_R |= 0x01;       // activate SSI0
    SYSCTL_RCGCGPIO_R |= 0x01;      // activate port A
    while((SYSCTL_PRGPIO_R&0x01) == 0){};// ready?
    GPIO_PORTA_AFSEL_R |= 0x2C;     // enable alt funct on PA2,3,5
    GPIO_PORTA_DEN_R |= 0x2C;       // configure PA2,3,5 as SSI
    GPIO_PORTA_PCTL_R = (GPIO_PORTA_PCTL_R&0xFF0F00FF)+0x00202200;
    GPIO_PORTA_AMSEL_R = 0;         // disable analog functionality on PA
    SSI0_CR1_R = 0x00000000;        // disable SSI, master mode
    SSI0_CPSR_R = 0x02;             // 8 MHz SSIClk 
    SSI0_CR0_R &= ~(0x0000FFF0);    // SCR = 0, SPH = 0, SPO = 0 Freescale
    SSI0_CR0_R |= 0x0F;             // DSS = 16-bit data
    SSI0_DR_R = data;               // load 'data' into transmit FIFO
    SSI0_CR1_R |= 0x00000002;       // enable SSI

}

void spiSend(uint16_t code){   
  while((SSI0_SR_R&0x00000002)==0){};// SSI Transmit FIFO Not Full
  SSI0_DR_R = code; // data out, no reply
}               

  
uint16_t spiSendAndReceive(uint16_t code){   
    uint16_t receive;
    while((SSI0_SR_R&0x00000002)==0){};// SSI Transmit FIFO Not Full
    SSI0_DR_R = code;                  // data out
    while((SSI0_SR_R&0x00000004)==0){};// SSI Receive FIFO Not Empty
    receive = SSI0_DR_R;               // acknowledge response
    return receive;
}


void portB_Init(void){ 
  volatile unsigned long delay;
  SYSCTL_RCGC2_R |= 0x00000002;   // activate clock for Port B
  delay = SYSCTL_RCGC2_R;   // allow time for clock to start
  GPIO_PORTB_AMSEL_R = 0x00;
  GPIO_PORTB_AFSEL_R = 0x00;
  GPIO_PORTB_PCTL_R = 0x00000000;
  GPIO_PORTB_DIR_R = 0x01;   // PB0 Output, PB1-7 Input 
  GPIO_PORTB_CR_R = 0xFF;
  GPIO_PORTB_PUR_R = 0xFE;
  GPIO_PORTB_DEN_R = 0xFF;   // enable digital port
}

    
int main(){
    SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);
    portB_Init();
    char buf[16];

    ConfigureUART();

    Lock lock;
    LockPin p1(1), p2(2), p3(3), p4(4), p5(5), p6(6), p7(7);
    lock.addPin(p1); lock.addPin(p2); lock.addPin(p3);
    lock.addPin(p4); lock.addPin(p5); lock.addPin(p6);
    lock.addPin(p7);
    lock.setCode(1);
    
    spiMaster_Init(546);
    volatile uint16_t reply;
    
    while(1){
        reply = spiSendAndReceive(9879);
        sprintf(buf, "%d", reply);
        UARTprintf(buf);
        UARTprintf("\n");
        
        /*
        sprintf(buf, "%d", lock.unlockSequence());
        UARTprintf(buf);
        UARTprintf("\n");
*/
    }
        
    return 0;
}