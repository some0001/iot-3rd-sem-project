# **PROJECT PLAN**

# Background

The project is based around IoT subject that is part of the UCL IT AP degree curriculum.

# Purpose

The purpose of the project is development of IoT system that uses SPI communication to communicate between various microcontrollers and uses WPAN for wireless data transmittion.

# Goals

The goal of this project is to build a digital lock system that uses zigbee WPAN for communication between the microchip managing the lock and Raspberry Pi that acts as a logging database and management device.
# Perspective

*  Gain knowledge and experience with programming of Arm Cortex based processors
*  Gain knowledge and experience on serial communication protocols - SPI and UART
*  Gain knowledge and experience with Zigbee networks and Zigbee devices

# Project implementation checklist

- [x] Write lock program for TM4C123GXL
- [x] Add spi communication to the TM4C123GXL program
- [ ] Program CC2650 Launchpad for Zigbee communication
- [ ] Add spi communication to the CC2650 Launchpad program
- [ ] Set-up raspberry Pi database
- [ ] Write Raspberry Pi program to manage settings and save records of TM4C123GXL 
- [ ] Connect all the devices together

# Overview of the system

![](./project_diagram.png)
